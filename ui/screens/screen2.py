import threading, time
from kivy.uix.screenmanager import  Screen
from urllib.parse import unquote_plus
from ui.screens.popup import P
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from kivy.uix.togglebutton import ToggleButton
from scraper_otimized import MapsSpider
from datetime import timedelta

class Screen2(Screen): 
    
    def detectar_nome(self):
        if self.ids['txtInput_name_url'].text.startswith('https://'):
            self.ids['txtInput_name_arquivo'].text = unquote_plus(
                self.ids['txtInput_name_url'].text
            ).split("/")[5].replace("+"," ").replace(" ","_") +'.csv'
        else:
            self.ids['txtInput_name_arquivo'
                     ].text = self.ids['txtInput_name_url'].text.replace(" ","_") +'.csv'
    
    def executar(self):
        self.p = P()        
        self.p.loading()
        self.p.open()
        threading.Thread(target=self._executar).start()
        
    def _executar(self):
        kwargs = {'export': 'csv',
                  'classification': list(filter(lambda x: x.state == 'down', ToggleButton.get_widgets("ordem")))[0].text.lower(),
                  'click_time': int(self.ids['sdr_click'].value),
                  'click_attempts': int(self.ids['sdr_click'].value/2),
                  'name_export': self.ids['txtInput_name_arquivo'].text.split(".")[0]}
        if self.ids['txtInput_name_url'].text.startswith('https://'):
            kwargs['url_search'] = unquote_plus(self.ids['txtInput_name_url'].text)
        else:
            kwargs['text_search'] = self.ids['txtInput_name_url'].text
        if self.ids['txtInput_limit'].text.isdigit():
            kwargs['limit_comment'] = int(self.ids['txtInput_limit'].text)
        else:
            kwargs['limit_comment'] = ''
        _time1 = time()
        instance = MapsSpider()
        instance._params = kwargs
        _result = instance.stalk()
        _time2 = time()
        if type(_result) is dict and _result.get('status') == 'success':
            print(_result)
            self.p.done(total=_result['message'], duration=timedelta(seconds=_time2-_time1))
        else:
            self.p.dismiss()
        
        #self.p.open()
        
        
    
    