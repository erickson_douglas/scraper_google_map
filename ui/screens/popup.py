from kivy.uix.popup import Popup
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.utils import get_color_from_hex as color_hex


class P(Popup):
    def __init__(self, **kwargs):
        super(P, self).__init__(**kwargs)    
    
    def loading(self):
        #self.title = "Baixando"
        _layout = RelativeLayout()
        _layout.add_widget(Image(source= "ui/src/logo.png",
                                 anim_delay= 0,
                                 mipmap= True,
                                 allow_stretch= True,
                                 pos_hint={'center_x':.5,'center_y':.8}))
        _layout.add_widget(Image(source= "ui/src/loading.gif",
                                 anim_delay= 0,
                                 mipmap= True,
                                 allow_stretch= True,
                                 pos_hint={'center_x':.5,'center_y':.25}))
        
        self.content = _layout
    
    def done(self, *, total=None, duration= None):
        _layout = RelativeLayout()
        _layout.add_widget(Image(source= "ui/src/logo.png",
                                 anim_delay= 0,
                                 mipmap= True,
                                 allow_stretch= True,
                                 pos_hint={'center_x':.5,'center_y':.8}))
        _layout.add_widget(Label(text=f"Extraido: {total}\nDuration: {str(duration).split('.')[0]}", 
                                 font_size=20,
                                 pos_hint={"center_x":0.5,"center_y": 0.45},
                                 size_hint= [.70, .08], halign='left', valign='top', 
                                 color=color_hex('#FFD700')))
        _btn = Button(text="Fechar",
                      pos_hint={'center_x':.5,'center_y':.25},
                      size_hint= [.70, .15])
        _btn.bind(on_press=lambda x: self.dismiss())
        _layout.add_widget(_btn)
        self.content = _layout