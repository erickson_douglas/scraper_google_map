from kivy.app import App
from kivy.uix.screenmanager import ScreenManager
from kivy.core.window import Window

from ui.screens.screen2 import Screen2
from ui.screens.gerenciamento import Gerenciamento


Window.clearcolor = (1,1,1,1)

class ScreenManagement(ScreenManager): pass
class ScraperUI(App): pass

if __name__ == '__main__':
    from kivy.config import Config
    Config.set('graphics', 'resizable', False) #0 being off 1 being on as in true/false
    Config.set('graphics', 'width', '766')
    Config.set('graphics', 'height','436')
    Config.write()
    ScraperUI().run()