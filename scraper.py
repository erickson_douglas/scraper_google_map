import scrapy,re
from scrapy.selector import Selector
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
#from selenium.common.exceptions import WebDriverException
from time import sleep
from bs4 import BeautifulSoup


class MapsSpider(scrapy.Spider):
    name = "maps_spider"
    start_urls = ["https://www.google.com/maps"]

    def parse(self, response):

        html = self.browser_selenium(text_search=getattr(self,'text_search', None),
                                     opcion_classification=getattr(self,'opcion_classification',None),
                                     url=getattr(self,'url',self.start_urls[0]),
                                     time_click=int(getattr(self,'time_click',5)),
                                     attempts_click=int(getattr(self,'attempts_click',5)),
                                     arch=getattr(self,'arch','linux'),
                                     limit=getattr(self,'limit',None))
        if html:
            soup = BeautifulSoup(html,"lxml")
            for line in soup.select("div.section-review.ripple-container div div.section-review-content"):
                if line.select("a div.section-review-title span"): name = line.select("a div.section-review-title span")[0].text
                if line.select("span.section-review-stars"): star = line.select("span.section-review-stars")[0].attrs['aria-label'].strip()[0]
                if line.select("a div.section-review-subtitle span"):
                    text = line.select("a div.section-review-subtitle span")[1].text
                    num_re = re.findall('[0-9]',text)
                    if num_re: rating = int("".join(num_re))
                    else: rating = text

                if line.select("span.section-review-text"):text = line.select("span.section-review-text")[0].text
                if name: yield {"Name": name, "Star": int(star), "Rating":rating, "Text": text}


    def browser_selenium(self,text_search,opcion_classification,url,time_click,attempts_click,arch,limit):
        def _pesquisa_local(count=0):
            print("Pesquisando o local")
            try:
                if count <attempts_click:
                    if url != self.start_urls[0]: pass
                    elif text_search:
                        result = browser.find_element_by_xpath('//*[@id="searchboxinput"]')
                        result.send_keys(text_search)
                        sleep(time_click)
                        result.send_keys(Keys.RETURN)
                        sleep(time_click*2)
                    else: sleep(time_click*3)
                    browser.refresh()
                    sleep(time_click*3)
                else: browser.close()

            except Exception as e:
                print(">Erro:",e)
                print(">>>Erro na pesquisa Local")
                sleep(time_click)
                _pesquisa_local(count=count+1)


        def _entrar_comentario(count=0):
            print("Clicando para mostrar os comentarios")
            try:
                if count <attempts_click:
                    if limit: self.limite = int(limit)
                    else: self.limite = int(browser.find_element_by_css_selector("div#content-container div#pane div.widget-pane.widget-pane-visible div.widget-pane-content.scrollable-y div.widget-pane-content-holder div.section-layout.section-layout-root div.jqnFjrOWMVU__root div.jqnFjrOWMVU__row div.jqnFjrOWMVU__right button.jqnFjrOWMVU__button.gm2-caption").text.split(" ")[0].replace(".",""))
                    print(self.limite)
                    browser.find_element_by_css_selector("div#content-container div#pane div.widget-pane.widget-pane-visible div.widget-pane-content.scrollable-y div.widget-pane-content-holder div.section-layout.section-layout-root div.jqnFjrOWMVU__root div.jqnFjrOWMVU__row div.jqnFjrOWMVU__right button.jqnFjrOWMVU__button.gm2-caption").click()
                else: browser.close()

            except Exception as e:
                print(">Erro:",e)
                print(">>>Erro ver comentarios do local")
                sleep(time_click)
                _entra_comentario(count=count+1)

        def _opcoes_classificacao(count=0):
            print("Escolhendo a opção de classficação")
            try:
                if count <attempts_click:
                    if opcion_classification:
                        browser.find_element_by_css_selector("div.jdmzw7a3xOb__section-dropdown-container").click()
                        target = browser.find_element_by_css_selector("div.section-dropdown-menu.section-dropdown-menu-vertical")
                        if opcion_classification   == "relevante": target.find_elements_by_css_selector("div")[0].click()
                        elif opcion_classification == "recente": target.find_elements_by_css_selector("div")[2].click()
                        elif opcion_classification == "maior": target.find_elements_by_css_selector("div")[4].click()
                        elif opcion_classification == "menor": target.find_elements_by_css_selector("div")[6].click()
                else: browser.close()

            except Exception as e:
                print(">Erro:",e)
                print(">>>Erro opcoes_classificacao")
                sleep(time_click)
                _opcoes_classificacao(count=count+1)

        def _todos_comentarios(count=0):# mostrar todos os comentarios
            print("Clicando até aparecer todos os comentarios")

            try:
                while True:
                    if count<attempts_click:
                        update = len(browser.find_elements_by_css_selector("div.section-review.ripple-container div div.section-review-content"))
                        print('---> durante:',update,'---> total:',self.limite)
                        if update>=self.limite: break
                        else:
                            browser.find_element_by_css_selector("html body.keynav-mode-off.screen-mode jsl div#app-container.vasquette.pane-open-mode div#content-container div#pane div.widget-pane.widget-pane-visible div.widget-pane-content.scrollable-y div.widget-pane-content-holder div.section-layout.section-layout-root div.section-layout.section-scrollbox.scrollable-y.scrollable-show").send_keys(Keys.PAGE_DOWN)
                            browser.find_element_by_css_selector("html body.keynav-mode-off.screen-mode jsl div#app-container.vasquette.pane-open-mode div#content-container div#pane div.widget-pane.widget-pane-visible div.widget-pane-content.scrollable-y div.widget-pane-content-holder div.section-layout.section-layout-root div.section-layout.section-scrollbox.scrollable-y.scrollable-show").send_keys(Keys.UP)
                    else: browser.close()

            except Exception as e:
                print(">>>Erro:",e)
                print(">>>Erro Mostrar todos os comentarios")
                sleep(time_click)
                _todos_comentarios(count=count+1)

        def _mensagem_completo(count=0):
            print("Clicando para mostrar a mensagem completo")
            try:
                if count<attempts_click:
                    for line in browser.find_elements_by_css_selector("html body.keynav-mode-off.screen-mode jsl div#app-container.vasquette.pane-open-mode div#content-container div#pane div.widget-pane.widget-pane-visible div.widget-pane-content.scrollable-y div.widget-pane-content-holder div.section-layout.section-layout-root div.section-layout.section-scrollbox.scrollable-y.scrollable-show div.section-layout div.section-review.ripple-container div div.section-review-content div.section-review-line jsl"):
                        if "Mais" in line.text: line.click()
                else: browser.close()

            except Exception as e:
                print(">Erro:",e)
                print(">>>Erro Ativar para mostrar conversa completa")
                sleep(time_click)
                _mensagem_completo(count=count+1)


        print("<Escolhendo o SO para abrir o navegador>")
        try:
            if "win" in arch.lower():
                print("browser Windows")
                binary  = FirefoxBinary('C:\\Program Files\\Mozilla Firefox\\firefox.exe')
                browser = webdriver.Firefox(firefox_binary=binary, executable_path=r'C:\\geckodriver.exe')
                browser.get(url)
            else:
                print("browser Linux")
                #browser = webdriver.PhantomJS()
                browser = webdriver.Firefox()
                browser.get(url)
        except Exception as e:
            print("Erro no SO")
            print("Erro:",e)
            return None


        print("<Executar os clicks>")

        _pesquisa_local()
        _entrar_comentario()
        _opcoes_classificacao()
        _todos_comentarios()
        _mensagem_completo()
        sleep(time_click)
        html = browser.page_source
        browser.close()
        return html
