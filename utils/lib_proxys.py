from requests import Session
from requests.adapters import HTTPAdapter
from urllib3 import Retry
from time import sleep
from random import choice
from requests_toolbelt.multipart.encoder import MultipartEncoder
from uuid import uuid4

OPTIONS = ['countwordsfree', 'online_convert', 'convertio']


class Proxys:
    def __init__(self):
        self._url = None
        self._session = Session()
        self._session.mount('http://', HTTPAdapter(max_retries=Retry(connect=3, backoff_factor=0.5)))
        self._session.mount('https://', HTTPAdapter(max_retries=Retry(connect=3, backoff_factor=0.5)))
        self._session.headers.update({"Cache-Control": "no-cache", "Pragma": "no-cache"})
        self._id_online_convert = {"id": None, "conversion": None}
        self._id_convertio = {"id": None, "conversion": None}

    def countwordsfree(self) -> str or None:
        res = self._session.post(url='https://countwordsfree.com:443/loadweb',
                                 headers={"Accept": "*/*",
                                          "Accept-Language": "pt-BR,en-US;q=0.7,en;q=0.3",
                                          "Accept-Encoding": "gzip, deflate",
                                          "Content-Type": "application/x-www-form-urlencoded",
                                          "X-Requested-With": "XMLHttpRequest",
                                          "Origin": "https://countwordsfree.com",
                                          "DNT": "1",
                                          "Connection": "close",
                                          "Referer": "https://countwordsfree.com/jsonviewer"},
                                 data={"url": self._url, "to": "to-txt"},
                                 timeout=None).json()
        if res['Success']:
            return res["Text"]
        return None

    def localhost(self) -> str or None:
        res = self._session.get(self._url).text
        if res:
            return res
        return None

    def online_convert(self) -> str or None:
        _url = "https://document.online-convert.com:443/api/jobs"
        _job = self._session.post(url=_url,
                                  data={"target": "txt", "category": "document", "fail_on_input_error": "false",
                                        "fail_on_conversion_error": "false", "process": "false"}).json()
        if _job.get('id'):
            self._id_online_convert['id'] = _job['id']
        if _job.get('conversion', [{}])[0].get('id'):
            self._id_online_convert['conversion'] = _job['conversion'][0]['id']
        self._session.post(url=f"{_url}/{self._id_online_convert['id']}/input",
                           data={"type": "remote", "source": self._url})
        self._session.post(url=f"{_url}/{self._id_online_convert['id']}/start",
                           data={"target": "txt", "category": "document", "language": "eng", "ocr_mode": "text",
                                 "string_method": "convert-to-txt",
                                 "conversion_id": self._id_online_convert['conversion']})
        while True:
            _temp = self._session.get(url=f"{_url}/{self._id_online_convert['id']}/callbackstatus").json()
            if _temp['status'] == 'completed':
                return self._session.get(_temp['output'][0]['uri']).text
            elif _temp['status'] == 'error':
                return None
            sleep(.5)

    def convertio(self, retries=0) -> str or None:
        if retries > 3:
            return None
        _url = 'https://convertio.co'

        self._session.get(_url)
        if self._session.get(f"{_url}/js/settings").json().get('session_id'):
            self._id_convertio['id'] = uuid4().hex
            self._id_convertio['conversion'] = self._session.get(f"{_url}/js/settings").json()['session_id']

        _multipart = MultipartEncoder(fields={'file_id': self._id_convertio['id'],
                                              'session_id': self._id_convertio['conversion'],
                                              'user_fn': f'URL:\n{self._url}',
                                              'user_fn_hash': 'ea6a55851a6c9a299666542ef594cee8',
                                              'file_source': 'html', 'file_hash': '0', 'file_out_format': 'TXT',
                                              'pack_id': '16e24f', 'send_gd_token': '', 'file_url': self._url})
        _upload_url = self._session.post(url=f'{_url}/process/upload_metadata', data=_multipart,
                                         headers={'Content-Type': _multipart.content_type}).text
        if not "http" == _upload_url[:4]:
            return self.convertio(retries=retries+1)
        self._session.post(f"{_url}/process/up_speed",
                           data={'file_id': self._id_convertio['id'], 'bps': '300'})
        _multipart = MultipartEncoder(fields={'file_id': self._id_convertio['id']})
        self._session.post(url=_upload_url, data=_multipart, headers={'Content-Type': _multipart.content_type})
        while True:
            resp = self._session.post(url=f'{_url}/process/get_file_status',
                                      data={'type': 'convert', 'id': self._id_convertio['id']}).json()
            if resp['convert'].get(self._id_convertio['id'], {}).get('out_url'):
                return self._session.get(resp['convert'][self._id_convertio['id']]['out_url']).text
            elif resp['convert'].get(self._id_convertio['id'], {}).get('error'):
                return None
            sleep(.5)

    def random(self, url) -> str or None:
        self._url = url
        _temp = choice(OPTIONS)
        try:
            return eval(f'self.{_temp}()')
        except Exception as e:
            print("Function:", _temp)
            print("Error:", e)
            return self.random(url=url)


if __name__ == "__main__":
    instance = Proxys()
    print(instance.convertio())