# scraper_google_map

scraper-google-maps é um bot que salva em csv, os comentários de lugares no google-maps

## Preparando o ambiente
- Firefox
- Geckodriver
- Git
### Linux
```sh
#abre o terminal e instale virtualenv e use o comando pra criar a pasta
$ virtualenv -p python3 scraper_maps
$ cd scraper_maps && source bin/activate
$ git clone https://gitlab.com/erickson_douglas/scraper_google_map.git
$ pip install scrapy selenium bs4
```
### Windows
#### [Python3]( https://dicasdepython.com.br/como-instalar-o-python-no-windows-10/)
#### [Firefox](https://support.mozilla.org/pt-BR/kb/como-baixar-e-instalar-firefox-windows)
#### [Geckodriver](https://medium.com/ananoterminal/ambientar-selenium-no-windows-3b880fa0e827)
#### [scraper_google_maps](https://gitlab.com/erickson_douglas/scraper_google_map/-/archive/master/scraper_google_map-master.zip)

```sh
# abre o cmd
$ cd scraper_google_map
$ pip install scrapy selenium bs4
```

## Exemplos
### Pesquisando pelo o nome do lugar
``` sh
$ python3 menu.py  --search_text " Barbearia Bruni"
```
### Pesquisando pelo o nome de 'n' lugares, separe por '||'
``` sh
$ python3 menu.py  --search_text " Barbearia Bruni||Drive Barbearia I Ipiranga"
```

### Pesquisando pelo arquivo de urls
``` sh
$ python3 menu.py  --search_url urls.txt
```
### Pesquisando pelo arquivo de urls
``` sh
$ python3 menu.py  --search_url urls.txt
```
### Escolhendo o Windows. (padrão Linux)
``` sh
$ menu.py  --search_text " Barbearia Bruni" --arch "windows"
```
### Adicionando o limite de comentarios, exemplo 4 mil.
``` sh
$ python3 menu.py  --search_url urls.txt --limit 4000
```
### Alterando o limite de tentativa dos clicks
``` sh
$ python3 menu.py  --search_text " Barbearia Bruni" --click_attempts 5
```
### Alterando o tempo dos clicks
``` sh
$ python3 menu.py  --search_text " Barbearia Bruni" --click_time 5
```
### Pesquisando 'n' lugares e adicionando no mesmo arquivo
``` sh
$ python3 menu.py  --search_text "  Barbearia Bruni||Drive Barbearia I Ipiranga" --name_file "barbearias"
```
