# coding: utf8
from re import compile as re_compile
from re import UNICODE as re_unicode
from ast import literal_eval
from time import sleep
from csv import DictWriter
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from utils.lib_proxys import Proxys
from os import name as os_name


CLASSIFICATION = { 'relevante': '2e4', 
                  'recente': '3e2', 
                  'maior': '3e3', 
                  'menor': '3e4'}


class MapsSpider:
    def __init__(self):
        self._browser = None
        self._url = 'https://www.google.com.br'
        self._cookies = {}
        self._params = None
        self._total_comments = None
        self._click_time = None
        self._click_attempts = None
        self._all_comments = None
        self._proxy = Proxys()
        self._url_split = None
        self._result = 0

    def _import_csv(self):
        fieldnames = ['Name', 'Date', 'Rating', 'Star', 'Text']
        _name_file = open(f'csv/{self._params.get("name_export", "temp")}.csv', 'w', newline='')
        self._all_comments = DictWriter(_name_file, fieldnames)
        self._all_comments.writeheader()

    @staticmethod
    def __clean_emoji(text):
        return re_compile("["
                          u"\U0001F600-\U0001F64F"  # emoticons
                          u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                          u"\U0001F680-\U0001F6FF"  # transport & map symbolsu"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags=re_unicode).sub(r'',text)
        
        
    def __ativar_selenium(self):
        print("[INFO] ativar selenium")
        if self._params.get('text_search'):
            self._browser.get(f"{self._url}/maps")
            return self.__pesquisa_local()

        elif self._params.get('url_search'):
            self._url = "/".join(self._params["url_search"].split("/")[:3])
            self._browser.get(f'{self._url}/{"/".join(self._params["url_search"].split("/")[3:])}')
            return self.__entrar_comentarios()
        else:
            return None

    def __pesquisa_local(self, retries=0):
        print("[INFO] Pequisa local selenium")
        try:
            if retries > self._click_attempts:
                return None

            result = self._browser.find_element_by_xpath('//*[@id="searchboxinput"]')
            result.send_keys(self._params.get('text_search'))
            sleep(self._click_time)
            result.send_keys(Keys.RETURN)
            return self.__entrar_comentarios()

        except Exception as e:
            print("error:", e)
            return self.__pesquisa_local(retries=retries + 1)

    def __entrar_comentarios(self):
        print("[INFO] entrando comentario")
        try:
            sleep(self._click_time)
            if not self._total_comments:
                if self._params.get('limit_comment'):
                    self._total_comments = int(self._params['limit_comment'])
                else:
                    self._total_comments = int(self._browser.find_element_by_xpath(
                        "//button[contains(@class,'gm2-caption')]").text[:-11].replace('.', '')
                                               )
            self._browser.find_element_by_xpath("//button[@aria-label='Ordenar']").click()
            self._browser.find_elements_by_xpath("//li[@class='action-menu-entry']")[-1].click()
            return self.__detectar_url()
        except Exception as e:
            print("error:", e)
            return None

    def __detectar_url(self):
        print("[INFO] Detectar url")
        sleep(self._click_time)
        try:
            _logs = self._browser.execute_script("return window.performance.getEntries();")
            self._cookies = dict(map(lambda x: (x['name'], x['value']), self._browser.get_cookies()))
            for log in reversed(_logs):
                if log['name'].startswith(f'{self._url}/maps/preview/review/listentitiesreviews'):
                    _logs = log['name']
                    break

            if type(_logs) == str:
                return _logs
            return None
        except Exception as e:
            print("error:", e)
            return None

    def __execute_url(self, url):
        print("[INFO] Executar_url")
        self._url_split = url.split('!')
        self._url_split[7] = CLASSIFICATION.get(self._params.get("classification"), '2e4')
        for enu, num in enumerate(range(10, self._total_comments + 10, 10), 1):
            if enu % 41 == 0:
                print(f'URL: enu{enu} - num{num}')
                url = self.__entrar_comentarios()
                if url:
                    self._url_split = url.split("!")
                    self._url_split[7] = CLASSIFICATION.get(self._params.get("classification"), '2e4')
            
            if num >= self._total_comments:
                self._url_split[5] = f'1i{self._total_comments-3}'
            else:
                self._url_split[5] = f'1i{num}'
            print("_url:", self._url_split)
            self.__map_request("!".join(self._url_split))
        return {'status': 'success'}

    def __map_request(self, url, *, retries=0):
        if retries > self._click_attempts:
            return None
        try:
            res = self._proxy.random(url=url)
            if res:
                list(
                    map(self.__map_json, literal_eval(res[5:].replace('null,', '').replace('\n', ''))[0])
                )
        except ConnectionError:
            sleep(self._click_time)
            return self.__map_request(url=url, retries=retries + 1)

    def __map_json(self, lists) -> bool:
        result = {'Name': lists[0][1], 'Date': lists[1]}
        if type(lists[2]) is str:
            result.update({'Text': self.__clean_emoji(lists[2]), 'Star': lists[3]})
            if type(lists[6]) is str:
                if type(lists[8][0][1]) is int:
                    result['Rating'] = lists[8][0][1]
                else:
                    result['Rating'] = None
            else:
                # result.update({'owner': {'Date': lists[6][0], 'Text': lists[6][1]}})
                if type(lists[9][0][1]) is int:
                    result['Rating'] = lists[9][0][1]
                else:
                    result['Rating'] = None
        else:
            result.update({'Text': None, 'Star': lists[2]})
            if type(lists[5]) is str:
                if type(lists[7][0][1]) is int:
                    result['Rating'] = lists[7][0][1]
                else:
                    result['Rating'] = None
            else:
                # result.update({'owner': {'Date': lists[5][0], 'Text': lists[5][1]}})
                if type(lists[8][0][1]) is int:
                    result['Rating'] = lists[8][0][1]
                else:
                    result['Rating'] = None
        self._all_comments.writerow(result)
        self._result +=1
        return True

    def stalk(self):
        try:
            # self._total_comments = 60
            # self.__map_request( 'https://www.google.com/maps/preview/review/listentitiesreviews?authuser=0&hl=pt-BR&gl=br&pb=!1m2!1y10722608419974508405!2y14974174086010320057!2m2!1i10!2i10!3e4!4m5!3b1!4b1!5b1!6b1!7b1!5m2!1sFDmBXobENNrC5OUPv7CoyAo!7e81')
            self._click_time = float(self._params.get("click_time", 5))
            self._click_attempts = float(self._params.get('click_attempts', 5))
            eval(f'self._import_{self._params.get("export", "csv")}()')
            option = Options()
            option.headless = False
            if os_name == "nt":
                _firefox = FirefoxBinary('C:\\Program Files\\Mozilla Firefox\\firefox.exe')
                self._browser = webdriver.Firefox(options=option, firefox_binary=_firefox, executable_path=r'utils\\geckodriver.exe')
            else:
                self._browser = webdriver.Firefox(options=option)
            url = self.__ativar_selenium()
            if url:
                self.__execute_url(url=url)
                return {'status': 'success','message': self._result}
        except Exception as e:
            print(e)
        finally:
            self._browser.close()


if __name__ == '__main__':
    kwargs = {
        'text_search': '',
        'url_search': 'https://www.google.com.br/maps/place/Cabeleireiro+Masculino/@-23.5858944,-46.7184099,17z/data=!3m1!4b1!4m5!3m4!1s0x94ce56fb2b6299ed:0xbd8515da127b98f!8m2!3d-23.5858944!4d-46.7162212',
        'classification': 'relevante',
        'click_time': 5,
        'click_attempts': 2,
        'limit_comment': None,
        'name_export': 'roosevelt',
        'export': 'csv'
    }

    instance = MapsSpider()
    instance._params = kwargs
    print(instance.stalk())
    pass
