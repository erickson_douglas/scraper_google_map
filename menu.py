import argparse, subprocess, time
from urllib.parse import unquote_plus

parser = argparse.ArgumentParser()
parser.add_argument("-st","--search_text",help="Pesquisar por nome, e pode ser uma lista, separe o nome com '||' ",type=str)
#parser.add_argument("-su","--search_url",help="Pesquisar por url, e pode ser uma lista, separe o nome com '||' ",type=str)
parser.add_argument("-su","--search_url_arq",help="Pesquisar um arquivo de lista de urls",type=str)
parser.add_argument("-oc","--opcion_classification",help="Selecionar a classificaçao, opções: [relevante,recente,maior,menor]",type=str)
parser.add_argument("-e","--export",help="nome do arquivo, de modo interativo",type=str)
parser.add_argument("--limit",help="Total de comentarios",type=int)
parser.add_argument("--name_file",help="Renomear o nome do arquivo",type=str)
parser.add_argument("--arch",help="Escolhe Sistema Operacional",type=str)
parser.add_argument("--click_time",help="Tempo de cada click",type=int)
parser.add_argument("--click_attempts",help="tentativa de cada click",type=int)

args = parser.parse_args()

if args.search_text:
    for line in args.search_text.split('||'):
        if args.name_file: name_file = args.name_file
        else: name_file = line
        command =["scrapy","runspider","scraper.py","-o","csv/{}.csv".format(name_file),"-a","text_search={}".format(line),'--nolog']

        if args.arch: command+= ["-a","arch={}".format(args.arch)]
        if args.opcion_classification: command+= ["-a","opcion_classification={}".format(args.opcion_classification)]
        if args.limit: command+= ["-a","limit={}".format(args.limit)]
        if args.click_time: command+= ["-a","time_click={}".format(args.click_time)]
        if args.click_attempts: command+= ["-a","attempts_click={}".format(args.click_attempts)]

        subprocess.run(command)
        time.sleep(5)

elif args.search_url_arq:
    with open(args.search_url_arq,"r") as readfile:
        for line in readfile.read().split("\n")[:-1]:
            if args.name_file: name_file = args.name_file
            else: name_file = unquote_plus(line).split("/")[5].replace("+"," ")
            print(name_file)
            command =["scrapy","runspider","scraper.py","-o","csv/{}.csv".format(name_file),"-a","url={}".format(line),'--nolog']

            if args.arch: command+= ["-a","arch={}".format(args.arch)]
            if args.opcion_classification: command+= ["-a","opcion_classification={}".format(args.opcion_classification)]
            if args.limit: command+= ["-a","limit={}".format(args.limit)]
            if args.click_time: command+= ["-a","time_click={}".format(args.click_time)]
            if args.click_attempts: command+= ["-a","attempts_click={}".format(args.click_attempts)]

            subprocess.run(command)
            time.sleep(5)

elif args.export:
    subprocess.run(["scrapy","runspider","scraper.py","-o","csv/{}.csv".format(args.export)])
    time.sleep(5)
else:
    subprocess.run(["python","menu.py","--help"])
